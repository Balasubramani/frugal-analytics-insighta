'use strict';

// custom modules
var collectService = require('../services/collect.service'),
    // NPM Modules
    access = require('safe-access');

exports.getIndex = function(req, res, next) {
    res.sendSuccess('success');
};

exports.create = function(req, res, next) {
    var modelData = access(req, 'body');
    collectService.create(modelData, function(dbError, data) {
        if (dbError) {
            res.sendDbError();
        } else {
            res.sendSuccess('Data processed successfully');
        }
    });
};

exports.login = function(req, res, next) {
    var modelData = access(req, 'query');
    collectService.login(modelData, function(dbError, data) {
        if (dbError) {
            res.sendDbError();
        } else {
            if (data.length) {
                res.sendSuccess('Data processed successfully');
            } else {
                res.sendNotFound();
            }
        }
    });
};

exports.getInteractions = function(req, res, next) {
    var trackingId = (req.params.trackingId).replace(':', '');
    collectService.getInteractions(trackingId, function(dbError, data) {
        if (dbError) {
            res.sendDbError();
        } else {
            if (data.length) {
                res.sendSuccess(data);
            } else {
                res.sendNotFound();
            }
        }
    });
};
