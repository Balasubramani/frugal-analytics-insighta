'use strict';

var mongoose = require('mongoose'),
    LoginModel,
    LoginSchema;

LoginSchema = new mongoose.Schema({
    username: {
        type: String,
        required: true
    },
    password: {
        type: String,
        required: true
    },
    createdAt: {
        type: Date,
        default: Date.now
    },
    updatedAt: {
        type: Date,
        default: Date.now
    }
}, {
    versionKey: false
});

LoginModel = mongoose.model('users', LoginSchema);

module.exports = LoginModel;
