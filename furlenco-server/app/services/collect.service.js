'use strict';

var mongoose = require('mongoose'),
    Cryptr = require("cryptr"),
    cryptr = new Cryptr('password'),
    MongoClient = require('mongodb').MongoClient,
    assert = require('assert'),
    _ = require('lodash'),
    url = 'mongodb://localhost:27017/flurencodedb',
    loginmodel = require('../models/login.model');

exports.create = function(data, next) {
    var collection = mongoose.connection.db.collection(data[':trackingId']);
    data._createdAt = new Date().toISOString();
    collection.insert(data, function(err, result) {
        if (!err)
            next(err, data);
        else
            next(result);
    });
};

exports.login = function(data, next) {
    loginmodel.find({
        username: data.username,
        password: cryptr.encrypt(data.password)
    }, function(dbError, data) {
        if (data.length)
            next(null, data);
        else
            next(dbError, data);
    });
};

var aggregateInteractions = function(db, trackingId, callback) {
    db.collection(trackingId).aggregate(
        [
            { $group: { "_id": "$interactionType", "value": { $sum: 1 } } }
        ]
    ).toArray(function(err, result) {
        var arrayValue = [];
        var len = result.length;
        for (var i = 0; i < len; i++) {
            arrayValue.push({
                label: result[i]._id,
                value: result[i].value
            });
        }
        callback(err, arrayValue);
    });
};

exports.getInteractions = function(trackingId, next) {
    MongoClient.connect(url, function(err, db) {
        assert.equal(null, err);
        aggregateInteractions(db, trackingId, function(err, res) {
            db.close();
            if (!err)
                next(null, res);
            else
                next(err);
        });
    });
};
