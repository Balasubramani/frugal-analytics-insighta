'use strict';

// NPM Modules
var Core = require('../controllers/collect.controller');

module.exports = function(app, router) {
    app.route('/collect')
        .post(Core.create);

    app.route('/login')
        .get(Core.login);

    app.route('/getinteractions:trackingId')
        .get(Core.getInteractions);

    app.use(router);
};
