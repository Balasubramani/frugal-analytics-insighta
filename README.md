# README #

This README would normally document whatever steps are necessary to get your application up and running.

### What is this repository for? ###

* Contains both the frontend and backend application in this repo. Furlenco Server, acts as a Backend server which collects data sent from analytics js from Furlenco Client app and process further for data visualization
* v1.0.0

### How do I get set up? ###

* Analytics JS, tracks user interactions and send to Backend server for further visualization.
* Clone the 'furlenco-server' project from here. https://bitbucket.org/Balasubramani/furlenco-server
* do 'npm install'
* Connect your MongoDB for local data processing
* do 'nodemon server'
* finally clone 'furlenco-client' and start browsing. https://bitbucket.org/Balasubramani/furlenco-client