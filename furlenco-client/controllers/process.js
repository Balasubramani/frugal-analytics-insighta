'use strict';

/*
* process.js
* It is used to handle the all events data and process it
*/

// process
function Process() {

}

// method event : arguments = sdkParams, eventType & callback
Process.prototype.event = function() {
    var defaultParams = arguments[0];
    var sdkParams = arguments[1];
    var eventType = arguments[2];
    // var callback = arguments[3];

    var helper = new Helper();
    // merging pageview parameters into SDK parameters
    sdkParams = helper.merge(defaultParams, sdkParams);

    sdkParams.interactionType = eventType;

    var ajax = new Request();
    // send data to Receiver
    ajax.send(sdkParams);
};


/*
    HELPER PART
*/
function Helper() {}

// merging two objects
Helper.prototype.merge = function(object1, object2) {
    if (arguments.length === 2 && (typeof object1 === 'object') && (typeof object2 === 'object')) {
        for(var attribute in object2) {
            try {
                object1[attribute] = object2[attribute];
            }
            catch(e) {
                console.log(e);
            }
        }
        return object1;
    } else {
        return object1;
    }
};

/*
 * REQUEST SECTION
 * It is used to send the SDK Parameters to Flurenco Server
 */

// Request Constructor
function Request() {
    this.url = 'http://localhost:4500/collect';
    // any variables goes here
}

Request.prototype.send = function() {
    // arguments
    var data = arguments[0]; //Getting parameter list
    // var callback = arguments[1]; //Getting user's callback
    var xmlhttp;

    xmlhttp = this.sendXMLHTTP(this.url, data);

    // Handling response from Receiver
    xmlhttp.onreadystatechange = function() {
        if (xmlhttp.readyState === 4 && xmlhttp.status === 200) {
            if (typeof callback === 'function') {
                callback(JSON.parse(xmlhttp.responseText));
            }
        }
    };
};

/**
 * @param  {[string]} url       [URL to make XMLHTTPREQUEST call]
 * @param  {[object]} eventData [The data to send to external URL]
 * @return {[object]}           [returns XMLHTTPREQUEST object]
 */
Request.prototype.sendXMLHTTP = function(url, eventData, data) {
    //Making HTTP Request to Receiver
    var xmlhttp;
    if (window.XMLHttpRequest) { // code for IE7+, Firefox, Chrome, Opera, Safari
        xmlhttp = new XMLHttpRequest();
    } else { // code for IE6, IE5
        xmlhttp = new ActiveXObject('Microsoft.XMLHTTP');
    }

    xmlhttp.open('POST', url, true);
    xmlhttp.setRequestHeader('Content-Type', 'application/json');

    xmlhttp.send(JSON.stringify(eventData));
    return xmlhttp;
};
